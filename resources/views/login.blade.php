@extends('layout.default')
@section('content')
<div class="container">
  <form class="form-signin" method="POST">
     {{ csrf_field() }}
    <input
      type="email"
      name="email"
      class="form-control"
      placeholder="Email address"
      required=""
      max=255
      autofocus="">
    <input
      type="password"
      name="password"
      class="form-control"
      placeholder="Password"
      min=8
      max=255
      required="">
    <div class="checkbox">
      <label>
        <input type="checkbox" name="remember-me" value="remember-me"> Remember me
      </label>
    </div>
    <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button><br>
    <div align="text-center">
       <div class="row">
         <div class="col-md-6">
            <button type="button" class="btn btn-lg btn-success btn-block">Signup</button>
         </div>
        <div class="col-md-6">
            <button type="button" class="btn btn-lg btn-warning btn-block">Reset Password</button>
         </div>
    </div>
  </form>
</div>
@stop