<style type="text/css">
  .chargeio-overlay .panel {top:150px !important;}
  label .form-control {margin-top:10px;}
  .full-width {width:100%}
  .panel.panel-default {margin:15px 0;}
  .mandatory-field {color:#f00;padding-left:5px;}
</style>
<div class="row box margin-none">
  <div class="innerAll inner-4x">
  <div class="modal fade" id="payNowModal" tabindex="-1"
  role="dialog" aria-labelledby="payNowModal"
  aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close"
          data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4>Invoice Payment</h4>
      </div>
      <div class="modal-body">
       <?php echo $this->renderPartial('paymentForm', array(
         'model' => $model->invoiceNumber,
         'paypalEmail' => $model->tenant->firmSettings->paypalEmail,
         ), false, false);?>
       </div>
     </div>
    </div>
  </div>
  <form action="<?=Yii::app()->controller->createUrl('invoice/pay')?>" class="form-horizontal" name="payment" role="form" method="POST">
    <ul class="errors list"></ul>
    <div class="form-group">
      <div class="col-sm-3">
        <label class="text-left full-width">Invoice Number<span class="mandatory-field">*</span>
          <?= CHtml::dropDownList(
            'invoice_number'
            ,$model->invoiceNumber->id
            ,CHtml::listData($model->invoices,'id','invoice_number')
            ,["required"=>"required","class"=>"form-control"]
            );
          ?>
        </label>
      </div>
      <div class="col-sm-3">
        <label class="text-left full-width">Payment Gateway<span class="mandatory-field">*</span>
          <select class="form-control" name="mode" required>
            <option value="" selected>Select Payment Gateway</option>
            <option
              value="LawPay"
                <?php if(!$model->invoiceNumber->tenant->firmSettings->chargeio_publicKey || !$model->invoiceNumber->tenant->firmSettings->chargeio_secretKey) : ?>
              disabled
                <?php endif; ?>
              >
              Lawpay
            </option>
            <option
              value="Paypal"
              <?php if(!$model->invoiceNumber->tenant->firmSettings->paypal_email) : ?>
              disabled
              <?php endif; ?>
            >Paypal</option>
          </select>
        </label>
      </div>
      <div class="col-sm-3">
        <label class="text-left full-width">Payment Type<span class="mandatory-field">*</span>
          <select class="form-control" name="type" required>
            <option value="" selected>Select Payment Type</option>
            <option value="card">Card</option>
            <option value="bank">Bank</option>
          </select>
        </label>
      </div>
      <div class="col-sm-2">
        <label class="text-left full-width">Amount (Due <?=Yii::app()->format->formatCurrency($model->invoiceNumber->dueAmount)?>)<span class="mandatory-field">*</span>
          <input class="form-control" type="number" step="0.01" name="amount_paid" placeholder="Please Enter Amount" max="<?=$model->invoiceNumber->dueAmount?>" required>
        </label>
      </div>
      <div class="col-sm-1">
        <label class="text-left full-width">Currency<span class="mandatory-field">*</span>
          <select class="form-control" name="currency" required>
            <option value="">Select Currency</option>
            <option value="USD" selected>USD</option>
          </select>
        </label>
      </div>
      <div class="clearfix"></div>
      <div id="card" class="payment_type" style="display:none">
        <div class="col-sm-12">
          <div class="panel panel-default">
            <div class="panel-heading">Credit Card Details</div>
          </div>
        </div>
        <div class="col-sm-3">
          <label class="text-left full-width">Card Number<span class="mandatory-field">*</span>
            <input class="form-control" type="number" step="1" name="number" placeholder="Please Enter Payment Amount" size="20" maxlength="20" required>
          </label>
        </div>
        <div class="col-sm-2">
          <label class="text-left full-width">Expiry Month<span class="mandatory-field">*</span>
            <input class="form-control" type="number" step="1" name="exp_month" placeholder="Expiry Month" size="2" maxlength="2" required>
          </label>
        </div>
        <div class="col-sm-2">
          <label class="text-left full-width">Expiry Year<span class="mandatory-field">*</span>
            <input class="form-control" type="number" step="1" name="exp_year" placeholder="Expiry Year" size="4" maxlength="4" required>
          </label>
        </div>
        <div class="col-sm-2">
          <label class="text-left full-width">CVV
            <input class="form-control" type="password" name="cvv" placeholder="CVV" maxlength="4" size="4">
          </label>
        </div>
        <div class="col-sm-3">
          <label class="text-left full-width">Card Holder\'s Name
            <input class="form-control" type="text" name="name" placeholder="Card Holder's Name" maxlength="70">
          </label>
        </div>
      </div>
      <div id="bank" class="payment_type" style="display:none">
        <div class="col-sm-12">
          <div class="panel panel-default">
            <div class="panel-heading">Bank Account Details</div>
          </div>
        </div>
        <div class="col-sm-3">
          <label class="text-left full-width">Bank Number<span class="mandatory-field">*</span>
            <input class="form-control" type="number" step="1" name="account_number" placeholder="Bank Number" size="20" maxlength="20" required>
          </label>
        </div>
        <div class="col-sm-3">
          <label class="text-left full-width">Bank Routing Number<span class="mandatory-field">*</span>
            <input class="form-control" type="routing_number" step="1" name="routing_number" placeholder="Bank Routing Number" size="10" maxlength="10" required>
          </label>
        </div>
        <div class="col-sm-3">
          <label class="text-left full-width">Bank Account Type<span class="mandatory-field">*</span>
            <select class="form-control" name="account_type" required>
              <option value="" selected>Select Bank Account Type</option>
              <option value="CHECKING">Checking</option>
              <option value="SAVINGS">Savings</option>
            </select>
          </label>
        </div>
        <div class="col-sm-3">
          <label class="text-left full-width">Account Holder's Name
            <input class="form-control" type="text" name="account_name" placeholder="Account Holder's Name" maxlength="70">
          </label>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="col-sm-12">
        <div class="panel panel-default">
          <div class="panel-heading">Personal Details</div>
        </div>
      </div>
      <div class="col-sm-6">
        <label class="text-left full-width">Address Line 1
          <input class="form-control" type="text" name="address1" placeholder="Address Line 1">
        </label>
      </div>
      <div class="col-sm-6">
        <label class="text-left full-width">Address Line 2
          <input class="form-control" type="text" name="address2" placeholder="Address Line 2">
        </label>
      </div>
      <div class="col-sm-2">
        <label class="text-left full-width">City
          <input class="form-control" type="text" name="city" placeholder="City">
        </label>
      </div>
      <div class="col-sm-2">
        <label class="text-left full-width">State
          <input class="form-control" type="text" name="state" placeholder="State">
        </label>
      </div>
      <div class="col-sm-2">
        <label class="text-left full-width">Postal Code
          <input class="form-control" type="number" name="postal_code" placeholder="Postal Code">
        </label>
      </div>
      <div class="col-sm-2">
        <label class="text-left full-width">Country
          <input class="form-control" type="text" name="country" placeholder="Country">
        </label>
      </div>
      <div class="col-sm-2">
        <label class="text-left full-width">Email Address
          <input class="form-control" type="email" name="email" placeholder="Email Address">
        </label>
      </div>
      <div class="col-sm-2">
        <label class="text-left full-width">Phone Number
          <input class="form-control" type="text" name="phone" placeholder="Phone Number">
        </label>
      </div>
      <div class="clearfix"></div>
      <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
        <input type="submit" class="btn btn-primary btn-block" value="Pay" name="submit"/>
      </div>
    </div>
  </form>
  <div class="modal fade" id="regPayModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"
          aria-label="Close"> 
          <span aria-hidden="true">&times;</span> 
        </button>
        <h4 id="paymentHeader"></h4>
      </div>
      <div class="modal-body"></div>
    </div>
  </div>
  </div>
<script type="text/javascript">
  function getFormData(selector,getDisabledValues) {
    var form = $(selector);
    if(getDisabledValues) {
      var disabled = form.find(':input:disabled').removeAttr('disabled');
    }
    var serialized = form.serializeArray();
    if(getDisabledValues) {
      disabled.attr('disabled','disabled');
    }
    return serialized;
  }
  function pay(url,data,method,context) {
    var ajaxParams = {};
    if(url) {
      ajaxParams.url = url;
    }
    if(data) {
      ajaxParams.data = data;
    }
    if(method) {
      ajaxParams.method = method;
      ajaxParams.type = method;
    }
    if(method) {
      ajaxParams.context = context
    }
    $.ajax(ajaxParams)
    .complete(function(xhr) {
      var http = {};
      http.status   = xhr.status;
      http.message  = xhr.statusText;
      http.response  = xhr.responseJSON;
      $(".has-error").remove();
      switch (xhr.status) {
        case 200: 
          BootstrapDialog.success(http.message);
        break;
        case 400:
        case 401:
        case 403:
        case 404:
        case 417:
        case 422:
        case 500:
          // if(xhr.getResponseHeader("Server-Validation")) {
            BootstrapDialog.alert("Whoops, something went wrong, please try again!");
            for(var i in http.response) {
              var err = http.response[i][0];
              if($("[name='"+i+"']").length && $("[name='"+i+"']").is(":visible")) {
                var msg = "<span class='has-error'>"+err+"</span>";
                $("[name='"+i+"']").after(msg);
              } else {
                $("form[name='payment']").find("ul.errors").append("<li class='has-error'>"+err+"</li>");
              }
            }
        break;
        default:
          BootstrapDialog.alert("Whoops, something went wrong, please try again!");
        break;
      }
    });
  }
  $(function() {
    $(":input").on("blur focus input keyup input change",function(event){
      var maxLength = $(this).attr("maxlength");
      if(maxLength) {
        if ($(this).val().length > maxLength) {  
            $(this).val($(this).val().substring(0, maxLength));  
        }  
      }
    });
    $(".payment_type").find(":input").attr('disabled','disabled');
    $(".chargeio").on("DOMNodeInserted","button.button.auto-width", function(){
      $(this).removeClass().addClass("btn btn-primary btn-block");
    })
    $("form[name='payment'] [name='type']").on("change keyup input",function(){
      var val = $(this).val();
      var sel = "#"+val;
      $paymentTypes = $(".payment_type");
      if(val) {
        $paymentTypes = $paymentTypes.not(sel);
      }
      $paymentTypes.find(":input").attr('disabled','disabled');
      $paymentTypes.hide();
      if(val) {
        $paymentMethod = $(sel);
        $paymentMethod.find(":input").removeAttr('disabled');
        $paymentMethod.slideDown("slow","swing");
      }
    });
    $("form[name='payment']").on("submit",function(event){
      event.preventDefault();
      var requestData = {};
      serialized = getFormData("form[name='payment']");
      $.each(serialized,function(i, field){
          requestData[field.name] = field.value;
      });
      var context = {element: $(this).find("[type='sumit']"),event:event};
      pay($(this).prop("action"),requestData,$(this).prop("method"),context);
      return;
    });
  });
</script>
