<!DOCTYPE html>
<html>
	<?php include'db_queries.php';?>
	<head>
	<style>table{display:none}</style>
		
	</head>
	
	<body>

</div>
		<div class="container">
		
			<header class="page-header">
				<h1>Bootstrap Implementation</h1><br>
				<h2>In Sample Join Operation Practice Web page</h2>
			</header>
			
			<div class="container-fluid">
			
			<form class="form-search">
  <input type="text" class="input-medium search-query">
  <button type="submit" class="btn">Search</button>
</form>
				
			<p>
  <button class="btn btn-primary" type="button" onclick="show_hide('add_employee')">ADD EMPLOYEES</button>
</p>	

			
			<div class="form-group" id="add_employee">			
				
				<form class="form-inline" role="form">
					<div class="form-group has-success has-feedback">
					
						<label class="control-label" for="inputSuccess4">Id:</label>
						<input type="text" class="form-control" id="inputSuccess4">
						<span class="glyphicon glyphicon-user form-control-feedback"></span>
						
						<label class="control-label" for="inputSuccess4">NAME:</label>
						<input type="text" class="form-control" id="inputSuccess4" placeholder="FirstName MiddleName LastName">
						<span class="glyphicon glyphicon-user form-control-feedback"></span>
						
						<label class="control-label" for="inputSuccess4">Designation:</label>
						<input type="text" class="form-control" id="inputSuccess4">
						<span class="glyphicon glyphicon-user form-control-feedback"></span>
						
					</div>
					<input type="submit">
				</form>
			</div>
			
			<button class="btn btn-large btn-primary" type="button" onclick="show_hide('basic_info')">SHOW BASIC INFO</button></p><p>
			
			
		<table id="basic_info" class="table table-striped table-hover" style="display:none">
				<caption>
				<div class="list-group">
					<a href="#" class="list-group-item active">
						<h4 class="list-group-item-heading">EMPLOYEE BASIC INFO</h4>
						<p class="list-group-item-text">ID, FULL NAME AND DESIGNATION</p>
					</a>
				</div>
			</caption>
				
				
				
					<tr>
						<th>Employee Id</th>
						<th>Employee Full Name</th>
						<th>Designation</th>
					</tr>
					<?php
					$query_type="SELECT";
					$table_name="employee_basic";
					$condition="";
					$columns_display="*";
					$a=array();
					$a=query_select($query_type,$columns_display,$table_name,$condition);
					foreach($a as $aa)
						{
							echo"<tr>";
								echo"<td>".$aa['id']."</td>";
								echo"<td>".$aa['first_name']." ".$aa['middle_name']." ".$aa['last_name']."</td>";
								echo"<td>".$aa['designation']."</td>";
							echo"</tr>";
						}
					?>
					
				</table>

  <button class="btn btn-large btn-primary" type="button" onclick="show_hide('advance_info')">SHOW MORE EMPLOYEE INFO</button></p><p>
				
		<table id="advance_info" class="table table-striped table-hover">
			<caption>
				<div class="list-group">
					<a href="#" class="list-group-item active">
						<h4 class="list-group-item-heading">EMPLOYEE EXTRA INFO</h4>
						<p class="list-group-item-text">
						ID, FULL NAME, TEAM LEADER, SUB TEAM LEADER, DATE OF JOININH AND POST</p>
					</a>
				</div>
			</caption>
					<tr>
						<th>Employee Id</th>
						<th>Employee Name</th>
						<th>Team Leader</th>
						<th>Sub Team Leader</th>
						<th>Date of Joining</th>
						<th>Post</th>
					</tr>
					<?php
					$query_type="SELECT";
					$table_name="employee_advance";
					$columns_display="*";
					$condition="";
					$a=array();
					$a=query_select($query_type,$columns_display,$table_name,$condition);
					foreach($a as $aa)
						{
							echo"<tr>";
								echo"<td>".$aa['id']."</td>";
								echo"<td>".$aa['name']."</td>";
								echo"<td>".$aa['tl']."</td>";
								echo"<td>".$aa['sub_tl']."</td>";
								echo"<td>".$aa['doj']."</td>";
								echo"<td>".$aa['post']."</td>";
							echo"</tr>";
						}
					?>
					
				</table>
		<button class="btn btn-large btn-primary" type="button" onclick="show_hide('comp_info')">
		SHOW COMPLETE EMPLOYEE INFO</button></p><p>
		
			<table id="comp_info" class="table table-striped table-hover">
					<tr>
						<th>Employee Id</th>
						<th>Employee Full Name</th>
						<th>Team Leader</th>
						<th>Designation</th>
						<th>Date of Joining</th>
						<th>Post</th>
					</tr>
					<?php
					$query_type="SELECT";
					$table_name="employee_basic;employee_advance";
					$columns_display="eb.id , eb.first_name , eb.middle_name , eb.last_name , ea.tl , eb.designation , ea.doj , ea.post ";
					$condition="eb.id=ea.id";
					$a=array();
					$a=query_select($query_type,$columns_display,$table_name,$condition);
					foreach($a as $aa)
						{
							echo"<tr>";
								echo"<td>".$aa['id']."</td>";
								echo"<td>".$aa['first_name']." ".$aa['middle_name']." ".$aa['last_name']."</td>";
								echo"<td>".$aa['tl']."</td>";
								echo"<td>".$aa['designation']."</td>";
								echo"<td>".$aa['doj']."</td>";
								echo"<td>".$aa['post']."</td>";
							echo"</tr>";
						}
					?>
					
				</table>
			

			</div>
			
			
  
  
			<footer>
				<h4>Copyright 2014</h4>
			</footer>
  
		<div class="container">
	</body>
</html>