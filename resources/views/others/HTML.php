<!DOCTYPE html>
<html><title>ULTIMATE WEBSITE</title>
<head><?php include '../constants.php';?></head>
<body>
<div id="page"><!--Page Div Start-->

    <!-- Always on top: Position Fixed-->
<div id="header_bakgnd"></div>
<div id="menu_bakgnd"></div>
<div id="footer_bakgnd"></div>
	<header>
		<span id="time">
			<?php echo date("d/m/Y");?>
			<br>
			<?php echo date("h:i:sa");?>
		</span>
		<h1><span>LOGIN / REGISTER</span></h1>
	</header>
	
	<?php include $php_ui_common_dir.'/menu.php';?>
    
	<!-- Fixed size after header-->

	<div class="content">
	
	<!-- Always on top. Fixed position, fixed width, relative to content width-->
		<div class="<?php echo $left_absolute_sidebar; ?>">sidebar-left</div>

	<!-- Scrollable div with main content -->
	<div id="scrollable2" class="html_cotent">
<h1>HYPERTEXT MARKUP LANGAUGE</h1>
<h1>Heading 1 [h1]</h1>
<h2>Heading 2 [h2]</h2>
<h3>Heading 3 [h3]</h3>
<h4>Heading 4 [h4]</h4>
<h5>Heading 5 [h5]</h5>
<h3>Heading 6 [h6]</h6>

<h3>HTML Text Formatting Tags</h3>
<p><b>This text is bold</b></p>
<p><big>This text is big</big></p>
<p><center>This text is in center</center></p>
<p><small>This text is small</small></p>
<p><ins>This text is inserted</ins></p>
<p><u>This text is underlined</u></p>
<p><del>This text</del><s>is</s><strike>deleted</strike></p>
<p><strong>This text is strong</strong></p>
<p><i>This text is italic</i></p>
<p><em>This text is emphasized</em></p>
<p><mark>This is text is mark</mark></p>
<p><tt>This is Teletype Text</tt></p>
<p>This is<sub> subscript</sub> and <sup>superscript</sup></p>

<h3>HTML "Computer Output" Tags</h3>
<code>This is a code</code><br>
<kbd>This is keyboard text</kbd><br>
<samp>This is sample computer code</samp><br>
<var>This is variable definition</var><br>

<h3>HTML Citations, Quotations, and Definition Tags</h3>

<abbr>This is an abbreviation/acronym</abbr>
<address>This is an address</address>
<bdo>This defines text direction</bdo><br>
<blockquote>This is a long Quotation</blockquote><br>
<q>This is an in-line (short) quotation</q><br>
<cite>This is a definition of title work</cite><br>
<dfn>This is a definition</dfn>
<blockquote>This is a section that is quoted from another source </blockquote>	
	
</div>

<!-- Always on top. Fixed position, fixed width, relative to content width -->
<div class="<?php echo $right_absolute_sidebar; ?>">sidebar-right</div>

</div><!-- Always at the end of the page -->

<?php include $php_ui_common_dir.'\footer.php';?>

</div><!--Page Div Ends--></body></html>