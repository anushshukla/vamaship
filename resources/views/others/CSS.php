<!DOCTYPE html>
<html><title>ULTIMATE WEBSITE</title>
<head><?php include '../constants.php';?></head>
<body>
<div id="page"><!--Page Div Start-->

    <!-- Always on top: Position Fixed-->
<div id="header_bakgnd"></div>
<div id="menu_bakgnd"></div>
<div id="footer_bakgnd"></div>
	<header>
		<span id="time">
			<?php echo date("d/m/Y");?>
			<br>
			<?php echo date("h:i:sa");?>
		</span>
		<h1><span>CSS</span></h1>
	</header>
	
	<?php include $php_ui_common_dir.'/menu.php';?>
    
	<!-- Fixed size after header-->

	<div class="content">
	<!-- Always on top. Fixed position, fixed width, relative to content width-->
		<div class="<?php echo $left_absolute_sidebar; ?>">sidebar-left</div>

	<!-- Scrollable div with main content -->
	<div id="scrollable2">
	
		<h2>CASCADING STYLE SHEET</h2>
		<h3>ALL PROPERTIES AND ITS VALUES GIVEN BELOW</h3>
		<h4>TYPES: INLINE | INTERNAL | EXTERNAL</h4>
		<h5>BASIC SYNTAX:- PROPERTY:VALUE<br>BUT FOR THE ABOVE THREE TYPES IT IS BELOW</h5>
		<h6>
			<ul>EXTERNAL CSS
				<li>html_tag_name 	: 	 </li> &nbsp
				<li>.class-name 	: 	 </li> &nbsp
				<li>#id_name 		:	 </li> &nbsp
				<li>*composite* 	:	 </li> &nbsp
			</ul>
		</h6>
		
	</div>

<!-- Always on top. Fixed position, fixed width, relative to content width -->
<div class="<?php echo $right_absolute_sidebar; ?>">sidebar-right</div>

</div><!-- Always at the end of the page -->

<?php include $php_ui_common_dir.'\footer.php';?>

</div><!--Page Div Ends--></body></html>