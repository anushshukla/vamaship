<!DOCTYPE html>
<html><title>ULTIMATE WEBSITE</title>
<head><?php include '../constants.php';?></head>
<body>
<div id="page"><!--Page Div Start-->

    <!-- Always on top: Position Fixed-->
<div id="header_bakgnd"></div>
<div id="menu_bakgnd"></div>
<div id="footer_bakgnd"></div>
	<header>
		<span id="time">
			<?php echo date("d/m/Y");?>
			<br>
			<?php echo date("h:i:sa");?>
		</span>
		<h1><span>LOGIN / REGISTER</span></h1>
	</header>
	
	<?php include $php_ui_common_dir.'/menu.php';?>
    
	<!-- Fixed size after header-->

	<div class="content">
	
	<!-- Always on top. Fixed position, fixed width, relative to content width-->
		<div class="<?php echo $left_absolute_sidebar; ?>">sidebar-left</div>

	<!-- Scrollable div with main content -->
	<div id="scrollable2" class="html_cotent">
		<h1>JSON</h1>
		<h2>JAVASCRIPT OBJECT NOTATION<h2>
		<h3>TYPES
			<ul>
				<li><a href="#" title="HTML TAG NAME">	OBJECT				{}		</a></li>
				<li><a href="#" title="CLASS NAME">		ARRAY				[]		</a></li>
				<li><a href="#" title="ID NAME">		OBJECT OF ARRAY		{[]}	</a></li>
				<li><a href="#" title="COMPOSITE">		ARRAY OF OBJECT		[{}]	</a></li>
				<li><a href="#" title="COMPOSITE">		OBJECT OF OBJECT	{{}}	</a></li>
				<li><a href="#" title="COMPOSITE">		ARRAY OF ARRAY		[[]]	</a></li>
			</ul>
		<h3>
	</div>

<!-- Always on top. Fixed position, fixed width, relative to content width -->
<div class="<?php echo $right_absolute_sidebar; ?>">sidebar-right</div>

</div><!-- Always at the end of the page -->

<?php include $php_ui_common_dir.'\footer.php';?>

</div><!--Page Div Ends--></body></html>