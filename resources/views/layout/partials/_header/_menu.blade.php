<ul class="nav flex-column nav-pills nav-fill nav-justified">
  <li class="nav-item">
    <a class="nav-link active" href="#">Dashboard</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#">Personal</a>
    <ul class="nav flex-column nav-pills nav-fill nav-justified">
      <li class="nav-item">
        <a class="nav-link" href="#">Credit Card</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Debit Card</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Bank Account</a>
      </li>
    </ul>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#">Contacts</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#">Accounting</a>
  </li>
  
</ul>