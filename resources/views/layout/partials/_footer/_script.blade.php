@yield('pre-script')
<script
	src="https://code.jquery.com/jquery-3.2.1.min.js"
	integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
	crossorigin="anonymous"></script>
<script
  src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
  integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
  crossorigin="anonymous"></script>
<script
  src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
  integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
  crossorigin="anonymous"></script>
<script
  src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
  integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
  crossorigin="anonymous"></script>
<script
	charset="UTF-8"
	type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"
	defer></script>

<script type="text/javascript">
  var GlobalObj = {}
	GlobalObj.regexList 	= {!! file_get_contents(public_path()."/js/regex.json") !!};
	var validateConfObj  	= {};
	$(document).ready(function(){
    @if(Session::has('error'))
      BootstrapDialog.alert({{Session::get('message')}});
    @elseif(Session::has('message'))
      BootstrapDialog.info({{Session::get('message')}});
    @endif
	});
</script>
@yield('script')
<!-- <script charset="UTF-8" type="text/javascript" src="{{url("js/routes.js")}}"></script>
<script charset="UTF-8" type="text/javascript" src="{{url("js/storage.js")}}"></script>
<script charset="UTF-8" type="text/javascript" src="{{url("js/style.js")}}"></script>
<script charset="UTF-8" type="text/javascript" src="{{url("js/ajax.js")}}"></script> -->

<script type="text/javascript">
	$(document).ready(function(e){
		/*$(".fancybox")
    	.fancybox({
        openEffect  : 'none',
        closeEffect : 'none',
        nextEffect  : 'none',
        prevEffect  : 'none',
        padding     : 0,
        margin      : [20, 60, 20, 60] // Increase left/right margin
    	});*/
	});
</script>
@yield('script')
