<div class="row">
    <div class="col-sm-12">
        <h3>Pages:</h3>
        <ul>
            <li><a href="#"><strong>SITE</strong></a></li>
            <ul>
                <li><a href="{{ route('site.about')}}">ABOUT US</a></li>
                <li><a href="{{ route('site.contact')}}">CONTACT US</a></li>
                <li><a href="{{ route('site.terms_of_use')}}">TERMS OF USE</a></li>
                <li><a href="{{ route('site.privacy_policy')}}">PRIVACY POLICY</a></li>
                <li><a href="{{ route('site.faq')}}">FAQ</a></li>
            </ul>
            <li><a href="#"><strong>OUR SERVICES</strong></a></li>
            <ul>
                <li><a href="#">Subpage 1</a></li>
                <li><a href="#">Subpage 2</a></li>
                <li><a href="#">Subpage 3</a></li><li><a href="#">Subpage 4</a></li>
            </ul>
            <li><a href="#"><strong>Portfolio</strong></a></li>
            <ul><li><a href="#">Subpage 1</a></li><li><a href="#">Subpage 2</a></li></ul>
            <li><a href="#"><strong>Contact</strong></a></li>
        </ul>   
    </div>
</div>

<div class="site-map">
    <div class="bottom uk-margin-large-top">
        <div class="uk-container uk-container-center uk-margin-top uk-margin-bottom">
            <div class="uk-grid">
                <div class="uk-width-large-1-4 uk-width-medium-1-2 uk-width-small-1-2">
                    <div class="heading">
                        <h3>OUR SERVICES</h3>
                    </div> 
                    <?php 
                        $links = [
                        /*,route("")=>"Business"
                        ,route("")=>"Commercial"*/
                        "login"=>"Residential"
                        /*,route("")=>"Inspection Report"
                        ,route("")=>"Appraisal Report"*/
                        ];
                        $li = [];
                        foreach ($links as $link => $text) :
                           $li[] = link_to_route($link,"<i class=\"uk-icon-angle-right\"></i> ".$text);
                        endforeach;
                    ?>
                    {!! Html::decode(Html::ul($li)) !!}                             
                </div>
                <div class="uk-width-large-1-4 uk-width-medium-1-2 uk-width-small-1-2">
                    <div class="heading">
                        <h3>Follow Us</h3>
                    </div> 
                    <div class="row">
                        <div class="col-sm-4 social-buttons">
                            @foreach(['adn'
                                , 'bitbucket'
                                , 'dropbox'
                                , 'facebook'
                                , 'flickr'
                                , 'foursquare'
                                , 'github'
                                , 'google'
                                , 'instagram'
                                , 'linkedin'
                                , 'microsoft'
                                , 'odnoklassniki'
                                , 'openid'
                                , 'pinterest'
                                , 'reddit'
                                , 'soundcloud'
                                , 'tumblr'
                                , 'twitter'
                                , 'vimeo'
                                , 'vk'
                                , 'yahoo'] as $name)
                                <a class="btn btn-block btn-social btn-{{$name}}">
                                    <i class="fa fa-{{$name}}"></i> Sign in with {{$name}}
                                </a>
                            @endforeach
                        </div>
                    </div>
                     <div class="row">
                        <div class="col-sm-4">
                            <button class="uk-button uk-button-primary tm-button-primary" data-uk-modal="{target:'#advertise-with-us'}">Advertise with us</button>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <p class="text-muted">&copy; {{env('DEFAULT_COPYRIGHT')}} {{env('DEFAULT_WEBSITE_NAME')}} All Rights Reserved.</p>
</div>
@include('layout.partials._footer._script')