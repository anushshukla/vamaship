@if(Route::currentRouteName())
<meta name="current-urn" content="{{route(Route::currentRouteName(),Route::current()->parameters())}}"/>
@endif
@if(Session::has("redirect.url"))
<meta http-equiv="refresh" content="5;{{Session::pull("redirect.url")}}" />
@endif

<meta name="_token" content="{!! csrf_token() !!}"/>
<meta name="environment" content="local">
<meta name="application-name" content="E-Commerce and Management Application">
<meta name="author" content="Anush Shukla">
<meta name="description" content="Buy / Sell / Rent anything and manage everything">
<meta name="revised" content="Buy / Sell / Rent anything and manage everything, 08/10/2016">
<meta name="generator" content="FrontPage 4.0">
<meta name="keywords" content="Buy, Sell, Rent, Management">
<meta name="robots" content="noindex, nofollow, noarchive, nosnippet">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="default-style" content="Bootstrap | Uikit | Foundation CSS">
<meta http-equiv="refresh" content="5000">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta http-equiv="cache-control" content="no-cache, no-store, must-revalidate">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="expires" content="0 | Tue, 01 Jan 1980 1:00:00 GMT">
<meta charset="UTF-8">
<meta name="today" content="{{@$now}}" scheme="ISO-8601|YYY-MM-DDTHH:mm:ss.sssZ">