<!DOCTYPE html>
<html>
	<head>
		<noscript>
			<meta http-equiv="refresh" content="0; url=google.com/search?q=enable+javascript+in+browser" />
		</noscript>
	    @include('layout.partials._head')
	</head>
	<body
		ng-app="vamaship"
		class="container @if(Route::currentRouteName() === 'cms.web.pages'){{Route::current()->parameters()['uri']}}@endif"
	>
	    <header>@include('layout.partials._header')<header>
	    <main>@yield('content')</main>
	    <footer>@include('layout.partials._footer')</footer>
	</body>
</html>