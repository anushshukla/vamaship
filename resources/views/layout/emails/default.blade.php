<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
 <html xmlns="http://www.w3.org/1999/xhtml">
 <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 </head>
 <body marginheight="0" marginwidth="0">
 <table width="600" border="0" cellpadding="0" cellspacing="0" style="margin:0 auto; border:1px solid #000;">
   @include('layout.emails.partials._emailheader')
   @yield('content')
   @include('layout.emails.partials._emailfooter')
 </table>

 </body>
 </html>