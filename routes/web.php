<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',['as'=>'home','uses'=> function () {
  return view('welcome');
}]);

Route::get('/login',['as'=>'login.view','uses'=> 'LoginController@loginView']);
Route::post('/login',['as'=>'login','uses'=> 'LoginController@login']);
Route::get('/logout',['as'=>'logout','uses'=> 'LoginController@logout']);

Route::get('/register',['as'=>'register','uses'=> function () {
  return view('register');
}]);

Route::get('/site/terms-of-use', ['as' => 'site.terms_of_use' ,'uses' => 'Site@terms_of_use']);
Route::get('/site/privacy-policy', ['as' => 'site.privacy_policy' ,'uses' => 'Site@privacy_policy']);
Route::get('/site/frequently-asked-questions', ['as' => 'site.faq' ,'uses' => 'Site@faq']);
Route::get('/site/about', ['as' => 'site.about' ,'uses' => 'Site@about']);
Route::get('/site/contact', ['as' => 'site.contact' ,'uses' => 'Site@contact']);

Route::group(array('prefix' => '', 'before' => 'auth'), function()
{
  // place your route definitions here
  Route::resource('user', 'UserController');
});
