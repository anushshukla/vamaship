# Apache2 Server Setup

Enable Apache2 Rewrite Module : sudo a2enmod rewrite
Enable Apache2 Rewrite SSL :sudo a2enmod ssl
Enable Apache2 Rewrite Proxy :sudo a2enmod proxy

# PHP

OpenSSL PHP Extension
<!-- PDO PHP Extension -->
sudo apt-get install php7.0-mysql
<!-- Mbstring PHP Extension -->
sudo apt-get install php7.0-mbstring
<!-- XML PHP Extension -->
sudo apt-get install php7.0-xml 

# MySQL Database Setup

mysql -u root -p
CREATE USER 'vamaship'@'localhost' IDENTIFIED BY 'password@vamaship.net';
create database `vamaship.net`
GRANT ALL PRIVILEGES ON * . * TO 'vamaship'@'localhost';
FLUSH PRIVILEGES;


php artisan migrate:refresh --seed
<!-- php artisan migrate:reset -->
<!-- php artisan migrate -->
<!-- php artisan db:seed -->
