<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CountriesTable::class);
        $this->call(UsersTableSeeder::class);
        $this->call(EmailsTableSeeder::class);
        $this->call(LoginTableSeeder::class);
    }
}
