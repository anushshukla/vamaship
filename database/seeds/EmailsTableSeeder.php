<?php

use Illuminate\Database\Seeder;

class EmailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      App\Emails::insert([
        ['email' => 'anush.shukla@gmail.com'],
        ['email' => 'anush.shukla@zelican.com'],
        ['email' => 'anush.shukla@vamaship.com']
      ]);
    }
}
