<?php

use Illuminate\Database\Seeder;

class LoginTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      App\Login::insert([
        ['user_id' => 1,'email_id' => 1, 'password' => Hash::make('123456')],
        ['user_id' => 1,'email_id' => 2, 'password' => Hash::make('123456')]
      ]);
    }
}
