<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('addresses');
        Schema::enableForeignKeyConstraints();
        Schema::create('addresses', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('line1', 256);
            $table->string('line2', 256);
            $table->string('landmark', 100)->nullable();
            $table->string('street', 100)->nullable();
            $table->string('zip', 100);
            $table->string('city', 100);
            $table->string('state', 100);
            $table->unsignedInteger('country_id');
            $table->index('country_id');
            $table->foreign('country_id')
              ->references('id')->on('countries')
              ->onDelete('cascade')
              ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
