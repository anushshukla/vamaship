<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /**
     * Handle an authentication attempt.
     *
     * @return Response
     */
    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|max:255',
            'password' => 'required|min:8|max:255',
        ]);
        if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')/*, 'active' => $request->input('verified')*/], $request->has('remember-me'))) {
            Auth::login($user, true);
            return redirect()->intended('home');
        }
    }

    /**
     * HTML view of the login webpage
     *
     * @return string
     */
    public function loginView()
    {
        return view('login');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('login');
    }
}
