<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SiteController extends Controller
{
  public function terms_of_use()
  {
    return view('terms_of_use');
  }
  public function privacy_policy()
  {
    return view('privacy_policy');
  }
  public function faq()
  {
    return view('faq');
  }
  public function about()
  {
    return view('about');
  }
  public function contact()
  {
    return view('contact');
  }
}
