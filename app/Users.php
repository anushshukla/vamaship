<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Users extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $dates = ['dob','created_at','updated_at','deleted_at'];

    public $rules = [
        'prefix' => 'nullable|max:255',
        'name' => 'required|max:255',
        'father_name' => 'nullable|max:255',
        'mother_name' => 'nullable|max:255',
        'maiden_name' => 'nullable|max:255',
        'surname' => 'nullable|max:255',
        'dob' => 'nullable|date',
    ];

    public function emails()
    {
        return $this->hasManyThrough('App\Emails', 'App\UsersEmailsMapping');
    }
    
    public function addresses()
    {
        return $this->hasManyThrough('App\Addresses', 'App\UsersAddressesMapping');
    }
}
